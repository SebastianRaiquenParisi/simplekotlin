fun main(args: Array<String>) {
    /*
    // strings
    /*
    var fullName: String
    val fullNameConst ="Donn Felker"

    println(fullNameConst::class)

    val firstName = "Donn"

    val tab: Char = '\t'
    val n: Char = '\n'
    val backSlash: Char = '\\'
    */
    // boolean
    */

    val message : String = """
        |Hello,
        |my name is Donn Felker,
        |How about you?
    """.trimMargin().replaceIndent("*-")

    println("the message is: \n$message\nIt has ${message.length} characters")

    /*

    // numbers.kt
    /*
    // val are constants
    val myByte:Byte=8 //8-bit
    val myShort:Short = 16 //16-bit
    val myInt:Int=32 //32-bit
    val myLong :Long = 64 //64-bit

    val myFloat:Float = 32.00F // 32-bit floating point number
    val myDouble:Double = 64.00// 64-bit floating point number

    // myInt-=2
    myInt.minus(2)
    // myInt+=2
    myInt.plus(2)

    println(myShort.toLong()::class)*/
    */

    var a = Person("Donn")
    var b = Person("Donn")
    println(a.equals(b))
    a = Person(null)
    var length = b.name?.length // null, 4
    length = if(b.name!=null) b.name?.length else 0 // 0, 4
    println(a.equals(b))
}
class Person(var name:String?){
    fun equals(other: Person): Boolean {
        return this.name == other.name
    }
}